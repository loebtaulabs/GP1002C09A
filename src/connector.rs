pub mod display;

#[derive(PartialEq)]
pub enum ConnectionVariant {
    //behavior specified on Manual page 15
    Z80,
    M6809,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum TriState {
    HIGH,
    LOW,
    Z,
}

const HIGH: bool = true;
const LOW: bool = false;
pub struct Connector {
    // GND and NC ports aren't being taken into account
    interface_variant: ConnectionVariant,
    pub interface: display::interface::Interface,
    address_lines: [bool; 16],
    data_lines: [TriState; 8],
    not_merq: bool,
    not_rd: TriState,
    not_wr: bool,
}

pub fn create_connector(
    interface_variant: &ConnectionVariant,
    memory_jumpers: (bool, bool, bool, bool),
) -> Connector {
    // Jumper settings described in manual page 8
    let interface = display::interface::create_interface(match memory_jumpers {
        (true, false, true, false) => 0,
        (false, true, true, false) => 0x4000,
        (true, false, false, true) => 0x8000,
        (false, true, false, true) => 0xC000,
        _ => panic!("Invalid Jumper positions!"),
    });

    match &*interface_variant {
        ConnectionVariant::Z80 => Connector {
            interface_variant: ConnectionVariant::Z80,
            interface: interface,
            address_lines: [LOW; 16],
            data_lines: [TriState::Z; 8],
            not_merq: HIGH,
            not_rd: TriState::HIGH,
            not_wr: HIGH,
        },
        ConnectionVariant::M6809 => Connector {
            interface_variant: ConnectionVariant::Z80,
            interface: interface,
            address_lines: [LOW; 16],
            data_lines: [TriState::Z; 8],
            not_merq: HIGH,
            not_rd: TriState::Z,
            not_wr: HIGH,
        },
    }
}

fn check_signal_validity(_connector: Connector) {
    if _connector.interface_variant == ConnectionVariant::Z80 {
        if _connector.not_rd == TriState::Z {
            panic!("Z80-Type interface, but NOT_RD disconnected!");
        }
    }
    if _connector.not_rd != TriState::Z {
        panic!("8609-Type interface, but NOT_RD connected!")
    }
}

pub fn change_mem_cell(_connector: &mut Connector, address: usize, value: u8) {
    _connector.interface.last_change += 0.000002;
    _connector.interface.memory[address - _connector.interface.memory_offset as usize]
        .push((value, _connector.interface.last_change));
}
