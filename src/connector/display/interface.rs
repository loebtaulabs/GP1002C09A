extern crate rand;
use rand::prelude::*;

pub struct Interface {
    pub memory_offset: u16,
    pub memory: Vec<Vec<(u8, f32)>>, // Memory of the module, combined with the time this change becomes effective at
    pub last_change: f32,
}

fn init_interface_memory() -> Vec<Vec<(u8, f32)>> {
    let mut ret: Vec<Vec<(u8, f32)>> = std::iter::repeat(vec![(0 as u8, 3 as f32)])
        .take(0x4000)
        .collect::<Vec<_>>();

    let mut rng = rand::thread_rng();
    /* initialize module memory randomly, to reflect real world.
    Last byte is automatically initialized to 0 on powerup,
    here it just doesn't get randomized
    */
    for i in 0..ret.len() - 1 {
        ret[i][0].0 = rng.gen();
    }
    ret
}

pub fn create_interface(memory_offset: u16) -> Interface {
    Interface {
        memory_offset: memory_offset,
        memory: init_interface_memory(),
        last_change: 0.0,
    }
}

pub fn get_current_display(_interface: &mut Interface, elapsed_time: f32) -> [(u8, f32); 0x4000] {
    let mut ret = [(0 as u8, 0 as f32); 0x4000];
    for i in 0.._interface.memory.len() {
        if _interface.memory[i].len() > 1 {
            for j in 0.._interface.memory[i].len() - 1 {
                if _interface.memory[i][j].1 - elapsed_time <= 0.0
                    && _interface.memory[i][j + 1].1 - elapsed_time <= 0.0
                {
                    _interface.memory[i].remove(j);
                }
            }
        }
        for j in 0.._interface.memory[i].len() {
            if _interface.memory[i][j].1 != 0.0 {
                _interface.memory[i][j].1 -= elapsed_time;
            }
            if _interface.memory[i][j].1 < 0.0 {
                _interface.memory[i][j].1 = 0.0;
            }
        }
        ret[i] = _interface.memory[i][0];
    }
    ret
}
